from pathlib import Path

ROOT_PATH = (Path(__file__).parent / "../../").resolve()
DATA_PATH = ROOT_PATH / "data"
WEIGHTS_PATH = ROOT_PATH / "weights"

print(WEIGHTS_PATH)
