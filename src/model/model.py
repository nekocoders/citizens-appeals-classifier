from collections import namedtuple
from typing import List

import torch
import tqdm
from transformers import AutoTokenizer, AutoModelForSequenceClassification

from src.common.extracted_themes import theme_group_by_themes
from src.common.paths import WEIGHTS_PATH

ClassOutput = namedtuple(
    "ClassOutput",
    [
        "theme",
        "theme_group",
        "executor",
        "theme_confidence",
        "theme_group_confidence",
        "executor_confidence",
    ],
)


class ClassifierModel:
    def __init__(self):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self._load_theme_model()
        self._load_executor_model()

    def _load_theme_model(self):
        model_id = WEIGHTS_PATH / "checkpoint-best-theme"

        self.tokenizer = AutoTokenizer.from_pretrained(model_id)
        self.model_theme = AutoModelForSequenceClassification.from_pretrained(model_id)
        self.model_theme.to(self.device)

    def _load_executor_model(self):
        model_id = WEIGHTS_PATH / "checkpoint-best-exec"

        self.model_exec = AutoModelForSequenceClassification.from_pretrained(model_id)
        self.model_exec.to(self.device)

    def classify(self, sentences: List[str], batch_size=16) -> List[ClassOutput]:
        all_predictions = []
        for i in tqdm.tqdm(
            range(0, len(sentences), batch_size), desc="Classifying sentences ..."
        ):
            batch = sentences[i : i + batch_size]
            inputs = self.tokenizer(
                batch,
                padding=True,
                truncation=True,
                max_length=512,
                return_tensors="pt",
            ).to(self.device)

            with torch.no_grad():
                logits_theme = self.model_theme(**inputs).logits
                logits_exec = self.model_exec(**inputs).logits

            scores_theme = torch.softmax(logits_theme, dim=-1)
            labels_theme = scores_theme.argmax(dim=-1)

            scores_exec = torch.softmax(logits_exec, dim=-1)
            labels_exec = scores_exec.argmax(dim=-1)

            for cls_theme, scr_theme, cls_exec, scr_exec in zip(
                labels_theme, scores_theme, labels_exec, scores_exec
            ):
                theme = self.model_theme.config.id2label[cls_theme.item()]
                score_theme = scr_theme[cls_theme].item()

                executor = self.model_exec.config.id2label[cls_exec.item()]
                score_exec = scr_exec[cls_exec].item()

                theme_group = theme_group_by_themes[theme]
                all_predictions.append(
                    ClassOutput(
                        theme=theme,
                        theme_group=theme_group,
                        executor=executor,
                        theme_confidence=score_theme,
                        theme_group_confidence=score_theme,
                        executor_confidence=score_exec,
                    )
                )
        return all_predictions
