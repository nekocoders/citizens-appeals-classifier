from pathlib import Path
from typing import List

import pandas as pd

from src.common.paths import DATA_PATH
from src.model.model import ClassifierModel


def accuracy(predictions: List[str], labels: List[str]) -> float:
    n_match = 0
    n_all = 0
    for lbl_pred, lbl_true in zip(predictions, labels):
        match = lbl_pred == lbl_true
        n_match += match
        n_all += 1
    return n_match / n_all


def write_results(
    csv_path: str | Path,
    ids: List[str],
    themes: List[str],
    theme_groups: List[str],
):
    ids = pd.DataFrame(ids, columns=["id"])
    themes = pd.DataFrame(themes, columns=["Тема"])
    theme_groups = pd.DataFrame(theme_groups, columns=["Группа тем"])
    data = pd.concat([ids, theme_groups, themes], axis=1)
    data.to_csv(csv_path, sep=";", index=False, encoding="utf-8")


def make_submit(csv_path: str | Path, save_path: str | Path = None):
    model = ClassifierModel()
    dataset = pd.read_csv(csv_path, sep=";")
    sentences = dataset["Текст инцидента"].tolist()
    ids = dataset["id"].tolist()

    predictions = model.classify(sentences)
    predictions = pd.DataFrame(
        predictions,
        columns=[
            "Тема",
            "Группа тем",
            "Исполнитель",
            "score_theme",
            "score_theme_group",
            "score_exec",
        ],
    )

    if save_path:
        write_results(
            save_path,
            ids,
            predictions["Тема"].tolist(),
            predictions["Группа тем"].tolist(),
        )


if __name__ == "__main__":
    make_submit(
        csv_path=DATA_PATH / "test.csv",
        save_path=DATA_PATH / "submission.csv",
    )
