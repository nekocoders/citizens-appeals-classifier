from pathlib import Path

import pandas as pd
from datasets import Dataset, ClassLabel


def load_dataset(
    csv_path: str | Path, label_column: str, text_column="Текст инцидента"
) -> Dataset:
    df = pd.read_csv(csv_path, encoding="utf-8", sep=";")
    labels = sorted(df[label_column].unique().tolist())
    class_labels = ClassLabel(num_classes=len(labels), names=labels)
    dataset = Dataset.from_pandas(df)
    dataset = dataset.map(
        lambda sample: {
            "label": class_labels.str2int(sample[label_column]),
            "text": sample[text_column],
        },
        remove_columns=list(df.keys()),
    )
    dataset = dataset.cast_column("label", class_labels)
    return dataset
