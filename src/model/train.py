import os.path
from pathlib import Path

from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, TrainingArguments, Trainer

from src.common.paths import WEIGHTS_PATH, ROOT_PATH, DATA_PATH
from src.model.dataset import load_dataset


def train_model(train_dataset_path: str | Path, label_column: str, batch_size=4):
    train_dataset = load_dataset(train_dataset_path, label_column=label_column)
    label2id = train_dataset.features["label"]._str2int
    id2label = {i: k for k, i in label2id.items()}

    MODEL_ID = WEIGHTS_PATH / "checkpoint-best"
    if not os.path.exists(MODEL_ID):
        MODEL_ID = "xlm-roberta-large"

    tokenizer = AutoTokenizer.from_pretrained(MODEL_ID)
    model = AutoModelForSequenceClassification.from_pretrained(
        MODEL_ID, num_labels=len(label2id), id2label=id2label, label2id=label2id
    )

    tokenized_train = train_dataset.map(
        lambda sample: tokenizer(sample["text"], truncation=True, max_length=512),
        batched=True,
    )

    training_args = TrainingArguments(
        output_dir=ROOT_PATH / "train_logs",
        overwrite_output_dir=True,
        learning_rate=2e-5,
        per_device_train_batch_size=batch_size,
        per_device_eval_batch_size=batch_size,
        num_train_epochs=2,
        weight_decay=0.01,
        evaluation_strategy="no",
        save_strategy="no",
        push_to_hub=False,
    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=tokenized_train,
        tokenizer=tokenizer,
    )

    trainer.train()

    trainer.save_model(WEIGHTS_PATH / "checkpoint-last")


if __name__ == "__main__":
    train_model(DATA_PATH / "train_dataset_filtered_train.csv", label_column="Тема")
