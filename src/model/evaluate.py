from pathlib import Path
from typing import List

import pandas as pd

from src.common.paths import DATA_PATH
from src.model.model import ClassifierModel


def accuracy(predictions: List[str], labels: List[str]) -> float:
    n_match = 0
    n_all = 0
    for lbl_pred, lbl_true in zip(predictions, labels):
        match = lbl_pred == lbl_true
        n_match += match
        n_all += 1
    return n_match / n_all


def write_results(
    csv_path: str | Path,
    sentences: List[str],
    themes: List[str],
    theme_groups: List[str],
    executors: List[str],
):
    themes = pd.DataFrame(themes, columns=["Тема"])
    theme_groups = pd.DataFrame(theme_groups, columns=["Группа тем"])
    executors = pd.DataFrame(executors, columns=["Исполнитель"])
    texts = pd.DataFrame(sentences, columns=["Текст инцидента"])
    data = pd.concat([executors, theme_groups, texts, themes], axis=1)
    data.to_csv(csv_path, index=False, sep=";")


def evaluate(csv_path: str | Path, save_path: str | Path = None):
    model = ClassifierModel()
    dataset = pd.read_csv(csv_path, sep=";")
    sentences = dataset["Текст инцидента"].tolist()

    predictions = model.classify(sentences)
    predictions = pd.DataFrame(
        predictions,
        columns=[
            "Тема",
            "Группа тем",
            "Исполнитель",
            "score_theme",
            "score_theme_group",
            "score_exec",
        ],
    )

    labels_theme = dataset["Тема"].tolist()
    labels_theme_group = dataset["Группа тем"].tolist()
    labels_executor = dataset["Исполнитель"].tolist()

    acc_theme = accuracy(predictions["Тема"].tolist(), labels_theme)
    acc_theme_group = accuracy(predictions["Группа тем"].tolist(), labels_theme_group)
    acc_exec = accuracy(predictions["Исполнитель"].tolist(), labels_executor)

    print(f"Accuracy theme       : {acc_theme*100:.1f}%")
    print(f"Accuracy theme group : {acc_theme_group*100:.1f}%")
    print(f"Accuracy executor    : {acc_exec*100:.1f}%")

    if save_path:
        write_results(
            save_path,
            sentences,
            predictions["Тема"].tolist(),
            predictions["Группа тем"].tolist(),
            predictions["Исполнитель"].tolist(),
        )


if __name__ == "__main__":
    evaluate(
        csv_path=DATA_PATH / "train_dataset_filtered_val.csv",
        save_path=DATA_PATH / "dataset_out.csv",
    )
