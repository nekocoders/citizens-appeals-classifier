# citizens-appeals-classifier

## Install

```
sudo apt install git-lfs
git clone https://gitlab.com/nekocoders/citizens-appeals-classifier.git
cd citizens-appeals-classifier
pip install -r requirements.txt
```
### Run server
```
python server/manage.py migrate
python server/manage.py runserver
```

## [Optional] Train and evaluate model

### Train
```
python3 src/model/train.py
```
new checkpoint will appear at `weights/checkpoint-last`

### Evaluate

```
python3 src/model/evaluate.py
```
Script will print accuracy by theme and theme group and save output to `data/dataset_out.csv`
