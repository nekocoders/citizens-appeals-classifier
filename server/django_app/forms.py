from django import forms


class SearchForm(forms.Form):
    search = forms.CharField(label='Search', max_length=100, required=False)


class AddAppealForm(forms.Form):
    text = forms.CharField(max_length=2000, help_text="Текст обращения")
    date = forms.DateField()


class EditAppealForm(forms.Form):
    appeal_id = forms.IntegerField(required=True)
    search_topic = forms.CharField(label='Search topic', max_length=100, required=False)
    search_topic_group = forms.CharField(label='Search topic', max_length=100, required=False)
    search_destination = forms.CharField(label='Search topic', max_length=100, required=False)

    topic_group = forms.CharField(max_length=2000, help_text="Группа тем")
    topic = forms.CharField(max_length=2000, help_text="Тема")
    destination = forms.CharField(max_length=2000, help_text="Исполнитель")
