from django.shortcuts import render

from src.common.extracted_themes import themes, theme_groups, departments
from src.model.model import ClassifierModel
from .forms import SearchForm, AddAppealForm, EditAppealForm
from .models import Appeal


classifier = ClassifierModel()


def _search_and_sort(search_string, string_list):
    # Фильтрация строк по вхождению подстроки
    filtered_list = [s for s in string_list if search_string.lower() in s.lower()]
    # Сортировка отфильтрованного списка по индексу вхождения строки поиска
    sorted_list = sorted(
        filtered_list, key=lambda s: s.lower().find(search_string.lower())
    )
    return sorted_list


def search_view(request):
    form = SearchForm(request.GET)
    results = None

    if form.is_valid():
        search_query = form.cleaned_data["search"]
        results = _search_and_sort(search_query, themes)

    return render(request, "search_results.html", {"form": form, "results": results})


def edit_appeal_view(request):
    form = EditAppealForm(request.GET)
    found_topics = themes[:10]
    found_topic_groups = theme_groups
    found_destinations = departments

    if form.is_valid():
        appeal = Appeal.objects.get(id=form.appeal_id)
        search_topic = form.cleaned_data["search_topic"]
        search_topic_group = form.cleaned_data["search_topic_group"]
        search_destination = form.cleaned_data["search_destination"]
        if search_topic:
            found_topics = _search_and_sort(search_topic, themes)

        if search_topic_group:
            found_topic_groups = _search_and_sort(search_topic_group, theme_groups)

        if search_destination:
            found_destinations = _search_and_sort(search_destination, departments)
        if not form.topic:
            form.topic = appeal.topic
        if not form.topic_group:
            form.topic = appeal.topic_group
        if not form.destination:
            form.destination = appeal.destination

    return render(
        request,
        "edit_appeal.html",
        {
            "form": form,
            "found_topics": found_topics,
            "found_topic_groups": found_topic_groups,
            found_destinations: found_destinations,
        },
    )


def add_appeal_view(request):
    form = AddAppealForm(request.GET)
    success = False
    new_appeal = None
    if form.is_valid():
        results = classifier.classify(sentences=[form.cleaned_data["text"]])[0]
        new_appeal = Appeal(
            text=form.cleaned_data["text"],
            date=form.cleaned_data["date"],
            topic=results.theme,
            topic_group=results.theme_group,
            destination=results.executor,
            topic_group_score=results.theme_group_confidence,
            topic_score=results.theme_confidence,
            destination_score=results.executor_confidence,
        )
        new_appeal.save()
        success = True

    return render(
        request,
        "add_appeal.html",
        {
            "form": form,
            "success": success,
            "appeal_text": new_appeal.text if new_appeal else "",
        },
    )


def list_appeals_view(request):
    topic = request.GET.get("topic")
    topic_group = request.GET.get("topic_group")

    if topic:
        results = Appeal.objects.filter(topic=topic).order_by("-id")[:10]
    elif topic_group:
        results = Appeal.objects.filter(topic_group=topic_group).order_by("-id")[:10]
    else:
        results = Appeal.objects.all().order_by("-id")[:10]

    return render(request, "list_appeals.html", {"results": results})


def start_page_view(request):
    return render(request, "start_page.html", {})
