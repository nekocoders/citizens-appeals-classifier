from django.db import models

# Create your models here.
from django.db import models
from django.utils.timezone import now


class Appeal(models.Model):
    text = models.CharField(max_length=2000, help_text="Текст обращения")
    topic_group = models.CharField(max_length=2000, help_text="Группа тем")
    topic_group_score = models.FloatField()
    topic = models.CharField(max_length=2000, help_text="Тема")
    topic_score = models.FloatField()
    destination = models.CharField(max_length=2000, help_text="Исполнитель")
    destination_score = models.FloatField()
    date = models.DateField(default=now)

    @property
    def print_topic_group_score(self):
        return f"{round(self.topic_group_score, 2) * 100}"

    @property
    def print_topic_score(self):
        return f"{round(self.topic_score, 2) * 100}"

    @property
    def print_topic_destination_score(self):
        return f"{round(self.destination_score, 2) * 100}"
